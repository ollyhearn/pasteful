from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os


DATABASE_URL = (
    os.environ['DIALECT'] + "+" +
    os.environ['DRIVER'] + "://" +
    os.environ['USER'] + ":" +
    os.environ['PASSWORD'] + "@" +
    os.environ['HOST'] + ":" +
    os.environ['PORT'] + "/" +
    os.environ['DB_NAME']
)


engine = create_engine(DATABASE_URL, echo=True)
Base = declarative_base()
session = sessionmaker(
    engine, expire_on_commit=False
)


def init_models():
    with engine.begin() as conn:
        conn.run_sync(Base.metadata.drop_all)
        conn.run_sync(Base.metadata.create_all)


# Dependency
def get_session() -> AsyncSession:
    with session() as s:
        yield s