from sqlalchemy import select
from sqlalchemy.orm import Session
from app.models import *


def get_biggest_cities(session: Session) -> list[City]:
    result = session.execute(select(City).order_by(City.population.desc()).limit(20))
    return result.scalars().all()


def add_city(session: Session, name: str, population: int):
    new_city = City(name=name, population=population)
    session.add(new_city)
    return new_city