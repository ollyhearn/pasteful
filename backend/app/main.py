from fastapi import FastAPI, Depends
from app.db.base import get_session
from sqlalchemy.orm import Session
from app.services import get_biggest_cities, add_city
from pydantic import BaseModel
import typer

app = FastAPI()
cli = typer.Typer()

class CitySchema(BaseModel):
    name: str
    population: int

    class Config:
        orm_mode = True



@cli.command()
def db_init_models():
    asyncio.run(init_models())
    print("Done")


@app.get("/")
async def root():
    return {"message": "Hello asadaa"}


@app.get("/test")
async def test(db: Session = Depends(get_session)):
    cities = get_biggest_cities(db)
    return [CitySchema(name=c.name, population=c.population) for c in cities]


@app.get("/create")
async def create(name: str = None, population: int = None, db: Session = Depends(get_session)):
    if name and population:
        new_city = add_city(db, name, population)
        db.commit()
        return {"message": "created"}
    return {"message": "not enough data provided"}