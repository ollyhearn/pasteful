fastapi
pydantic
uvicorn
psycopg2-binary
sqlalchemy
alembic
watchfiles
typer